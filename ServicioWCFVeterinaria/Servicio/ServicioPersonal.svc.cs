﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioPersonal" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioPersonal.svc o ServicioPersonal.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioPersonal : IServicioPersonal
    {
        public List<personal> getPersonals()
        {
            try
            {
                List<personal> Listpersonal = new List<personal>();
                var model = new ModelVeterinario();
                Listpersonal = model.personals.ToList();
                return Listpersonal;
            }
            catch (Exception) { }
            return new List<personal>();
        }


        public personal getPersonal(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.personals.SingleOrDefault(p => p.id == id);
            }
            catch (Exception) { }
            return new personal();
        }


        public Boolean AddPersonal(personal newPersonal)
        {
            try
            {
                var model = new ModelVeterinario();
                model.personals.Add(newPersonal);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }


        public Boolean ModifyPersonal(personal newPersonal)
        {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newPersonal).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }
    }
}
