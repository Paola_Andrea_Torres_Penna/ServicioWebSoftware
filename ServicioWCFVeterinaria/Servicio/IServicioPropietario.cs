﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicioWCFVeterinaria.Model;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioPropietario" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioPropietario
    {
        [OperationContract]
        [WebGet(UriTemplate = "Propietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<propietario> getPropietarios();

        [OperationContract]
        [WebGet(UriTemplate = "Propietario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        propietario getPropietario(int id);

        [OperationContract]
        [WebInvoke(UriTemplate = "Propietario", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddPropietario(propietario newPropietario);


        [OperationContract]
        [WebInvoke(UriTemplate = "Propietario", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyPropietario(propietario newPropietario);
    }
}
