﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioUsuario" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioUsuario.svc o ServicioUsuario.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioUsuario : IServicioUsuario
    {
        public List<usuario> getUsuarios()
        {
            try
            {
                List<usuario> Listusuario = new List<usuario>();
                var model = new ModelVeterinario();
                Listusuario = model.usuarios.ToList();
                return Listusuario;
            }
            catch (Exception) { }
            return new List<usuario>();
        }


        public usuario getUsuario(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.usuarios.SingleOrDefault(p => p.id == id);
            }
            catch (Exception) { }
            return new usuario();
        }


        public Boolean AddUsuario(usuario newUsuario)
        {
            try
            {
                var model = new ModelVeterinario();
                model.usuarios.Add(newUsuario);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }


        public Boolean ModifyUsuario(usuario newUsuario)
        {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newUsuario).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }
    }
}
