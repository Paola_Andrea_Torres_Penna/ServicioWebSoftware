﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioWCFVeterinaria.Model;
using System.Data.Entity;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ServicioHistorial" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioHistorial.svc o ServicioHistorial.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServicioHistorial : IServicioHistorial
    {

        public List<historial> getHistorials()
        {
            try
            {
                List<historial> Listhistorial = new List<historial>();
                var model = new ModelVeterinario();
                Listhistorial = model.historials.ToList();
                return Listhistorial;
            }
            catch (Exception) { }
            return new List<historial>();
        }


        public historial getHistorial(int id)
        {
            try
            {
                var model = new ModelVeterinario();
                return model.historials.SingleOrDefault(p => p.id == id);
            }
            catch (Exception) { }
            return new historial();
        }


        public Boolean AddHistorial(historial newHistorial)
        {
            try
            {
                var model = new ModelVeterinario();
                model.historials.Add(newHistorial);
                model.SaveChanges();
                return true;
            }
            catch (Exception) { }
            return false;
        }


        public Boolean ModifyHistorial(historial newHistorial)
        {
            try
            {
                var model = new ModelVeterinario();
                model.Entry(newHistorial).State = EntityState.Modified;
                return true;
            }
            catch (Exception) { }
            return false;
        }

    }
}
