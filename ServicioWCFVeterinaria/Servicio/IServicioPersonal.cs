﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicioWCFVeterinaria.Model;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioPersonal" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioPersonal
    {

        [OperationContract]
        [WebGet(UriTemplate = "Personal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<personal> getPersonals();

        [OperationContract]
        [WebGet(UriTemplate = "Personal/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        personal getPersonal(int id);

        [OperationContract]
        [WebInvoke(UriTemplate = "Personal", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddPersonal(personal newPersonal);


        [OperationContract]
        [WebInvoke(UriTemplate = "Personal", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyPersonal(personal newPersonal);
        

    }
}
