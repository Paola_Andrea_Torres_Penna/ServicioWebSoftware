﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ServicioWCFVeterinaria.Model;

namespace ServicioWCFVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IServicioMascota" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IServicioMascota
    {
        [OperationContract]
        [WebGet(UriTemplate = "Mascota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<mascota> getMascotas();
        
        [OperationContract]
        [WebGet(UriTemplate = "Mascota/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        mascota getMascota(int id);
                [OperationContract]
        [WebInvoke(UriTemplate = "Mascota", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean AddMascota(mascota newMascota);


        [OperationContract]
        [WebInvoke(UriTemplate = "Mascota", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyMascota(mascota newMascota);
        

    }
}
